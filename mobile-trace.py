#!/usr/bin/env python

import frida
import sys
import codecs
import subprocess
from termcolor import colored
import binascii

version = 0.1
line = "-" *80

ANDROID_PLATFORM = "android"
IOS_PLATFORM = "ios"

ANDROID_PATH = "hooks/android/{0}"
IOS_PATH = "hooks/ios/{0}"

logfile=None


def process_payload(payload):
    output = []
    output.append("[*] {0}".format(line))
    if 'timestamp' in payload:
        output.append("[*] Timestamp: {0}".format(payload['timestamp']))
    if 'hook' in payload:
        output.append("[*] Hook: {0}".format(payload['hook']))
    if 'lib' in payload:
        output.append("[*] lib: {0}".format(payload['lib']))
    if 'msg' in payload:
        output.append("[*] Message: {0}".format(payload['msg']))
    if 'params' in payload:
        params = []
        for x in payload['params']:
            params.append("[*] {0}:\t{1}".format(x, payload['params'][x]))
        output.append("[*] Params: \n{0}".format("\n".join(params)))
    if 'raw' in payload:
        raw = [];
        for x in payload['raw']:
            raw.append("[*] {0} (HEX):\t{1}".format(x, payload['raw'][x]))
            try:
                a_str = binascii.unhexlify(payload['raw'][x])
                raw.append("[*] {0} (ASCII):\t{1}".format(x, a_str))
            except:
                pass
        output.append("[*] Raw Data: \n{0}".format("\n".join(raw)))
    if 'retval' in payload:
        output.append("[*] Retval: {0}".format(payload['retval']))
    output.append("[*] {0}".format(line))
    return output


def print_payload(output=[]):
    try:
        print "\n".join(output)
    except:
        pass


def write_payload(output=[]):
    try:
        with open(logfile, "a") as f:
            f.write("\n".join(output))
    except Exception as e:
        print e


def on_message(message, data):
    """
    Callback function for Javascript Hooks.

    :param message: message with JSON content.
    :param data: RAW data (optional)
    :return:
    """
    if message['type'] == 'send':
        output = process_payload(message['payload'])
        print_payload(output)
        if logfile is not None:
            write_payload(output)
    elif message['type'] == 'error':
        print colored(message['stack'], 'red')


def read_scripts(filenames=[]):
    """
    This function reads all files specified in the filename list and merge them into one string.

    :param filenames: List with filename to read
    :return: content of all files as a single string
    :rtype: str
    """
    print filenames
    sources = []
    for fname in filenames:
        with codecs.open(fname, 'r', 'utf-8') as f:
            source = f.read()
            sources.append(source)
    return "\n".join(sources)


def parse_scriptnames(scripts, platform):
    """
    Parse script names, specified under the option <pre>-s</pre> or <pre>--script</pre>. Multiple filenames can be
    seperated with ','. Just the name of the script is needed (no path like 'hooks/android/').

    :param scripts: list of scriptnames that should be injected into the process
    :param platform: platform of the target device
    :return:
    """
    results = []
    scripts = scripts.split(",")
    scripts = [x for x in scripts if x != '']
    for script in scripts:
        if not script.endswith(".js"):
            script += ".js"
        if platform == ANDROID_PLATFORM:
            results.append(ANDROID_PATH.format(script))
        if platform == IOS_PLATFORM:
            results.append(IOS_PATH.format(script))
    return results


def main(proc, platform, scripts, usb_device=False, hide_root=False, filename=None):
    script_names = ['hooks/helper.js']
    if hide_root and platform == ANDROID_PLATFORM:
        script_names.append(ANDROID_PATH.format("HideRoot.js"))

    script_names.extend(parse_scriptnames(scripts,platform))

    logfile = filename
    try:
        if usb_device:
            session = frida.get_usb_device().attach(proc)
        else:
            if platform == ANDROID_PLATFORM:
                subprocess.call(["adb forward tcp:27042 tcp:27042"], shell=True)
            session = frida.get_remote_device().attach(proc)
        print colored("[+] Attached to process {0}".format(proc), 'green')
    except Exception as e:
        print colored("Error: {0}".format(e), 'red')
        sys.exit(1)

    try:
        # merge scripts into one string
        source = read_scripts(script_names)

        # inject scripts into process
        script = session.create_script(source)
        script.on('message', on_message)
        script.load()

        # keep script running
        sys.stdin.read()
    except KeyboardInterrupt as e:
        print colored("Process interrupted by User. Good Bye!", 'red')
    except Exception as e:
        print colored("Error: {0}".format(e), 'red')
    finally:
        session.detach()
        sys.exit(1)


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(prog="art", version=version,
                                     description="asfasfasf")
    parser.add_argument("-U", "--usb-device", required=False, action="store_true",
                        help="Connect to USB device instead of remote device")
    parser.add_argument("-p", "--platform", required=False, default=ANDROID_PLATFORM,
                        help="Platform of the target device. This must be 'android' or 'ios'")
    parser.add_argument("-r", "--hide-root", required=False, action='store_true', help="Include HideRoot.js script")
    parser.add_argument("-a", "--attach-process", required=False, default=None, help="Attach process by name or PID")
    parser.add_argument("-s", "--scripts", required=False, default="",help="one or more script names")
    parser.add_argument("-o", "--out-file", required=False, default=None, help="Name of the logfile to use")
    args = parser.parse_args()

    main(args.attach_process, args.platform, args.scripts, args.usb_device, args.hide_root, args.out_file)