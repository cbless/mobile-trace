'use strict';


Interceptor.attach(Module.findExportByName("libcommonCrypto.dylib", 'CCCryptorUpdate'), {

    onEnter: function (log, args, state) {
        console.log("CCCryptorUpdate() ");
        var data = {};
        data.timestamp = new Date();
        data.hook = "CCCryptorUpdate";
        data.lib = "libcommonCrypto.dylib";
        data.msg = "Hook: CCCryptorUpdate(cryptorRef: " + args[0] + " dataIn: " + args[1] +
                    " dataInLength: " + args[2] + " dataOut: " + args[3] +" dataOutLength: " + args[4] +
                    " &dataOutMoved: " + args[5] + ")"

        var params = {};
        params.dataIn = args[1];
        params.dataInLength = args[2].toInt32();
        params.dataOut = args[3];
        params.dataOutLength = args[4].toInt32();
        params.dataOutMoved = args[5];

        var raw = {}
        if (params.dataIn && (params.dataInLength > 16) ) {
            try {
                var buf = toHexString(Memory.readByteArray(this.dataIn, this.dataInLength));
                raw.dataInBuf = buf;
            } catch (e) {

            }
        }
        data.raw = raw;
        data.params = params;
        this.data = data

    },

    onLeave: function (log, retval, state) {
        if (this.data.params.dataOutLength && this.data.params.dataOutLength> 16){
            try{
                var buf = toHexString(Memory.readByteArray(this.dataOut, this.dataOutLength));
                this.data.raw.dataOutBuf = buf;
            }catch (e) {

            }
        }
        this.data.retval = retval;
        send(this.data)
    }

});



Interceptor.attach(Module.findExportByName("libcommonCrypto.dylib", "CCCryptorCreate"), {

    onEnter: function (log, args, state) {
        console.log("CCCryptorCreate ()" );
        var data = {};
        data.timestamp = new Date();
        data.hook = "CCCryptorCreate";
        data.lib = "libcommonCrypto.dylib";

        var params = {};
        params.op = args[0].toInt32();
        params.alg = args[1].toInt32();
        params.options = args[2].toInt32();
        params.key = args[3];
        params.keyLength = args[4].toInt32();
        params.iv = args[5];

        params.strKey = toHexString(Memory.readByteArray(key, keyLength));
        data.params = params;

        send(data);
    }

});

