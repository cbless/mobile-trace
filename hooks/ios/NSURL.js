'use strict';

if (ObjC.available) {

    var urlWithString = ObjC.classes.NSURL['+ URLWithString:'];

    Interceptor.attach(urlWithString.implementation, {
        onEnter: function (args) {
            var data = {};
            data.timestamp = new Date();
            data.hook = "URLWithString";
            data.lib = "NSURL";

            var url = ObjC.Object(args[2]).toString();
            data.msg = url;

            send(data);
        },
    });

    var initWithString = ObjC.classes.NSURL['- initWithString:'];

    Interceptor.attach(initWithString.implementation, {
        onEnter: function (args) {
            var data = {};
            data.timestamp = new Date();
            data.hook = "initWithString";
            data.lib = "NSURL";

            var url = ObjC.Object(args[2]).toString();
            data.msg =  url;

            send(data);
        },
    });

    var nsURLRequest = ObjC.classes.NSURLRequest;
    //console.log(nsURLRequest.$methods);
    var reqInit = ObjC.classes.NSURLRequest['- initWithURL:'];

    Interceptor.attach(reqInit.implementation, {

        onEnter: function (args) {
            var data = {};
            data.timestamp = new Date();
            data.hook = "initWithString";
            data.lib = "NSURLRequest";

            var url = ObjC.Object(args[2]).toString();
            data.msg =  url;

            send(data);
        },

    });



}
