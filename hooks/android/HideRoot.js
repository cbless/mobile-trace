'use strict';

var bad_packages = [
    /* known Root App packes. */
    "com.noshufou.android.su",
    "com.noshufou.android.su.elite",
    "eu.chainfire.supersu",
    "com.koushikdutta.superuser",
    "com.thirdparty.superuser",
    "com.yellowes.su",
    /* knownDangerousAppsPackages */
    "com.koushikdutta.rommanager",
    "com.dimonvideo.luckypatcher",
    "com.chelpus.lackypatch",
    "com.ramdroid.appquarantine",
    /* knownRootCloakingPackages */
    "com.devadvance.rootcloak",
    "de.robv.android.xposed.installer",
    "com.saurik.substrate",
    "com.devadvance.rootcloakplus",
    "com.zachspong.temprootremovejb",
    "com.amphoras.hidemyroot",
    "com.formyhm.hideroot"
];

var binary_locations = [
    "/data/local/",
    "/data/local/bin/",
    "/data/local/xbin/",
    "/sbin/",
    "/system/bin/",
    "/system/bin/.ext/",
    "/system/bin/failsafe/",
    "/system/sd/xbin/",
    "/system/usr/we-need-root/",
    "/system/xbin/"
];

var bad_binaries = ["su", "busybox"];

if (Java.available) {
    Java.perform(function () {
        var f = Java.use("java.io.File");

        f.exists.implementation = function () {
            var data = {};
            data.timestamp = new Date();
            data.hook = "exists()";
            data.lib = "java.io.File";

            var file_path = this.path['value'];
            var original_result = this.exists();

            if (original_result){
                for (var i = 0; i < binary_locations.length; i++) {
                    for (var j = 0 ; j < bad_binaries.length; j++){
                        var fname = binary_locations[i] + bad_binaries[j];
                        if (fname === file_path) {
                            data.msg = "Access to " + this.path['value'] + " detected! Hide file.";
                            send(data);
                            return false;
                        }
                    }
                }
            }
            return original_result;
        }
    });



    // TODO: Test it
    Java.perform(function() {
        var pm = Java.use("android.content.pm.PackageManager");
        pm.getPackageInfo.implementation = function(packageName, flags) {
            var data = {};
            data.timestamp = new Date();
            data.hook = "getPackageInfo()";
            data.lib = "android.content.pm.PackageManager";

            for (var i = 0 ; i < bad_packages; i++){
                if (packageName === bad_packages[i]){
                    data.msg = "Access to package " + packageName + " detected. Hide package.";
                    send(data);
                    throw pm.NameNotFoundException.$new("Package not found");
                }
            }
        }
    });


    Java.perform(function(){
        var jstring = Java.use("java.lang.String");

        jstring.contains.implementation = function(s){
            var data = {};
            data.timestamp = new Date();
            data.hook = "contains()";
            data.lib = "java.lang.String";
            //console.log(s);
            if (s === "test-keys" || s === "ro.debuggable" || s === "ro.secure") {
                data.msg = "Lookup for string '" +s + "' detected. Hide string.";
                send(data);
                return false;
            }
            for (var i = 0 ; i < bad_packages; i++){
                if (s === bad_packages[i]){
                    data.msg = "Lookup for string '" +s + "' detected. Hide string.";
                    send(data);
                    return false;
                }
            }

            return this.contains(s);
        }
    });

} // if Java.available

