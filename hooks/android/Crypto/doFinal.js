'use strict';


var ENCRYPT_MODE = 0x00000001;
var DECRYPT_MODE = 0x00000002;
var WRAP_MODE = 0x00000003;
var UNWRAP_MODE = 0x00000004;


var PUBLIC_KEY = 0x00000001;
var PRIVATE_KEY = 0x00000002;
var SECRET_KEY = 0x00000003;


Java.perform(function () {
    var Cipher = Java.use("javax.crypto.Cipher");

    /*
    * byte[] doFinal()
    * @see https://developer.android.com/reference/javax/crypto/Cipher.html#doFinal()
    */
    Cipher.doFinal.overload().implementation = function(){
        console.log("doFinal()");
        return this.doFinal.overload().apply(this, arguments);
    }


    /*
    * byte[] doFinal(byte[] input)
    * @see https://developer.android.com/reference/javax/crypto/Cipher.html#doFinal(byte[])
    */
    Cipher.doFinal.overload("[B").implementation = function(){
        var data = {};
        data.timestamp = new Date();
        data.hook = "doFinal(byte[] input)";
        data.lib = "javax.crypto.Cipher";
        data.msg = "doFinal(byte[] input )"

        var raw = {};
        raw.input = toHexString(arguments[0]);


        var retval =  this.doFinal.overload("[B").apply(this, arguments);
        raw.retval = toHexString(retval);

        data.raw = raw;
        send(data);
        return retval;
    }


    /*
    * int doFinal(byte[] output, int outputOffset)
    * @see https://developer.android.com/reference/javax/crypto/Cipher.html#doFinal(byte[],%20int)
    */
    Cipher.doFinal.overload("[B", "int").implementation = function(){
        console.log("doFinal(byte[] output, int outputOffset)");
        return this.doFinal.overload("[B", "int").apply(this, arguments);
    }

    /*
    * int doFinal(byte[] input, int inputOffset, int inputLen, byte[] output)
    * @see https://developer.android.com/reference/javax/crypto/Cipher.html#doFinal(byte[],%20int,%20int,%20byte[])
    */
    Cipher.doFinal.overload("[B", "int", "int", "[B").implementation = function(){
        console.log("doFinal(byte[] output, int outputOffset, int inputLen, byte[] output)");
        return this.doFinal.overload("[B", "int", "int", "[B").apply(this, arguments);
    }

    /*
    * int doFinal(byte[] input, int inputOffset, int inputLen, byte[] output, int outputOffset)
    * @see https://developer.android.com/reference/javax/crypto/Cipher.html#doFinal(byte[],%20int,%20int,%20byte[],%20int)
    */
    Cipher.doFinal.overload("[B", "int", "int", "[B", "int").implementation = function(){
        console.log("doFinal(byte[] output, int outputOffset, int inputLen, byte[] output, int outputOffset)");
        return this.doFinal.overload("[B", "int", "int", "[B", "int").apply(this, arguments);
    }

    /*
    * int doFinal(byte[] input, int inputOffset, int inputLen, byte[] output, int outputOffset)
    * @see https://developer.android.com/reference/javax/crypto/Cipher.html#doFinal(byte[],%20int,%20int,%20byte[],%20int)
    */
    Cipher.doFinal.overload("java.nio.ByteBuffer", "java.nio.ByteBuffer").implementation = function(){
        console.log("doFinal(ByteBuffer input, ByteBuffer output)");
        return this.doFinal.overload("java.nio.ByteBuffer", "java.nio.ByteBuffer").apply(this, arguments);
    }
});