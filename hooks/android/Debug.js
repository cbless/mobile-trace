'use strict';


Java.perform(function () {
    var debug = Java.use("android.os.Debug");

    debug.isDebuggerConnected.implementation = function () {
        var data = {};
        data.timestamp = new Date();
        data.hook = "isDebuggerConnected()";
        data.lib = "android.os.Debug";
        send(data);
        return false;
    }
});